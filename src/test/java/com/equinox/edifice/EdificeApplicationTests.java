package com.equinox.edifice;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.equinox.edifice.models.User;
import com.equinox.edifice.services.UserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class EdificeApplicationTests {

	@Autowired
	private UserService usserService;
	
	@Test
	public void contextLoads() {
	}
	
	@Test
	public void testUserLoad(){
		User user = new User();
		user.setFirstName("Equinox Admin");
		user.setLastName("EQ");
		user.setUserName("Equinox_Admin");
		user.setPassword("Equinox_Passw0rd");
		user.setRole("Admin");
		User newuser =  usserService.register(user);
		assertNotNull(newuser.getId());
	}

}
