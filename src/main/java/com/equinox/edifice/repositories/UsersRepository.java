package com.equinox.edifice.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.equinox.edifice.models.User;

@Repository
public interface UsersRepository extends JpaRepository<User, Integer> {

	List<User> findByUserNameAndPassword(String username, String password);
}
