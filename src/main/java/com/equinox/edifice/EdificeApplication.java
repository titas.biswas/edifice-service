package com.equinox.edifice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EdificeApplication {

	public static void main(String[] args) {
		SpringApplication.run(EdificeApplication.class, args);
	}
}
