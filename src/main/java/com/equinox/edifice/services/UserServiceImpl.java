package com.equinox.edifice.services;

import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.equinox.edifice.models.User;
import com.equinox.edifice.repositories.UsersRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UsersRepository userRepo;

	@Override
	public User register(User newUser) {
		newUser.setPassword(DigestUtils.sha256Hex(newUser.getPassword()));
		return userRepo.save(newUser);
	}

	@Override
	public User matchUser(String userName, String password) {
		List<User> users=  userRepo.findByUserNameAndPassword(userName, DigestUtils.sha256Hex(password));
		return users.get(0);
	}

	@Override
	public User getByUserName(String userName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<User> findAll() {
		return userRepo.findAll();
	}

	
	
}
