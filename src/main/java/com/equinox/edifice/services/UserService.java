package com.equinox.edifice.services;

import java.util.List;

import com.equinox.edifice.models.User;

public interface UserService {

	User register(User newUser);
	User matchUser(String userName, String password);
	User getByUserName(String userName);
	List<User> findAll();
}
