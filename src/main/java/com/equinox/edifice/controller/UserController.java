package com.equinox.edifice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.equinox.edifice.models.User;
import com.equinox.edifice.services.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping
	public ResponseEntity<List<User>> getAllUser(){
		System.out.println("In UserController:getAllUser");
		 return ResponseEntity.ok(userService.findAll());
	}
	
	@GetMapping("/{userName}")
	public ResponseEntity<User> getUserById(@PathVariable String userName){
		 return ResponseEntity.ok(userService.getByUserName(userName));
	}
	
	@PostMapping("/register")
	public ResponseEntity<User> register(@Valid @RequestBody User user){
		return ResponseEntity.ok(userService.register(user));
	}
	
	@PostMapping("/authenticate")
	public ResponseEntity<User> authenticate(@Valid @RequestBody User user){
		return ResponseEntity.ok(userService.matchUser(user.getUserName(), user.getPassword()));
	}

}
